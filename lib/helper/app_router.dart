import 'package:auto_route/auto_route.dart';
import 'package:project_web/screens/dashboard/dashboard_page.dart';
import 'package:project_web/screens/home_screens/home_page.dart';
import 'package:project_web/screens/login_screens/login_page.dart';
import 'package:project_web/screens/users/users_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(
      path: '/home',
      page: HomePage,
      maintainState: true,
      children: [
        CustomRoute(
          path: 'dashboard',
          page: DashBoardPage,
          initial: true,
          transitionsBuilder: TransitionsBuilders.fadeIn,
          maintainState: false,
        ),
        CustomRoute(
          path: 'users',
          page: UsersPage,
          transitionsBuilder: TransitionsBuilders.fadeIn,
          maintainState: false,
        ),
        // AutoRoute(path: 'settings', page: SettingsPage),
      ],
    ),
    AutoRoute(path: '/login', page: LoginPage, initial: true)
  ],
)
class $AppRouter {}
