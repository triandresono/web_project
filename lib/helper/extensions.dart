import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:project_web/helper/app_router.gr.dart';
import 'package:project_web/helper/constant.dart';

extension SnackBar on BuildContext {
  dynamic succesSnackbar(String message) {
    ScaffoldMessenger.of(this).showSnackBar(succesSnackbar(message));
  }

  dynamic failSnackbar(String message) {
    ScaffoldMessenger.of(this).showSnackBar(failSnackbar(message));
  }
}

extension Routers on BuildContext {
  dynamic replaceNamed(String path) {
    AutoRouter.of(this).replaceNamed(path);
  }

  dynamic homeRoute(PageRouteInfo<dynamic> path) {
    if (kIsWeb && !isWebMobile) {
      (this).navigateTo(HomeRoute(children: [path]));
    } else {
      (this).pushRoute(HomeRoute(children: [path]));
    }
    // (this).router.removeLast();
  }
}
