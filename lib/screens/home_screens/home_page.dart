import 'package:auto_route/auto_route.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:project_web/helper/constant.dart';
import 'package:project_web/shared_widgets/nav_headers.dart';
import 'package:project_web/shared_widgets/persist_drawer.dart';
import 'package:project_web/theme/colors.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        backgroundColor: projectBG,
        appBar: jagaBar(),
        body: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Visibility(
              visible: kIsWeb && !isWebMobile,
              child: const PersistDrawer(),
            ),
            const Expanded(child: AutoRouter()),
          ],
        ));
  }
}
