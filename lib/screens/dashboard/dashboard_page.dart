import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:project_web/helper/constant.dart';
import 'package:project_web/screens/dashboard/dashboard_cards.dart';
import 'package:project_web/shared_widgets/bar_example/horizontal_chart.dart';
import 'package:project_web/shared_widgets/common_widgets.dart';
import 'package:project_web/shared_widgets/nav_drawer.dart';
import 'package:project_web/shared_widgets/nav_headers.dart';
import 'package:project_web/theme/colors.dart';
import 'package:project_web/theme/padding.dart';
import 'package:project_web/helper/app_scale.dart';

class DashBoardPage extends StatefulWidget {
  const DashBoardPage({Key? key}) : super(key: key);

  @override
  _DashBoardPageState createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: navDrawer(),
      appBar: navHeader('Dashboard'),
      backgroundColor: projectBG,
      body: const DashboardBody(),
    );
  }
}

class DashboardBody extends StatefulWidget {
  const DashboardBody({Key? key}) : super(key: key);

  @override
  _DashboardBodyState createState() => _DashboardBodyState();
}

class _DashboardBodyState extends State<DashboardBody> {
  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: projectOutterPadded,
        height: context.deviceHeight() * 3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: SizedBox(
                width: context.deviceWidth(),
                child: const ResponsiveList(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      child: DashboardCard(
                        color: projectOldGreen,
                        icon: Icons.check_circle_outline,
                        numbers: '5',
                        text: 'Approve',
                      ),
                    ),
                    Expanded(
                      child: DashboardCard(
                        color: projectOldYellow,
                        icon: Icons.warning_rounded,
                        numbers: '2',
                        text: 'Revisi',
                      ),
                    ),
                    Expanded(
                      child: DashboardCard(
                        color: projectOldRed,
                        icon: Icons.close,
                        numbers: '5',
                        text: 'Ditolak',
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: (kIsWeb && !isWebMobile) ? 3 : 2,
              child: const Chart(),
            ),
            Spacer(flex: (kIsWeb && !isWebMobile) ? 8 : 4),
            // Expanded(
            //   child: BarChartSample5(),
            // ),
            // Spacer(flex: 2),
          ],
        ),
      ),
    );
  }
}
