import 'package:project_web/helper/app_scale.dart';
import 'package:flutter/material.dart';
import 'package:project_web/theme/colors.dart';
import 'package:project_web/theme/icons.dart';

class DashboardCard extends StatelessWidget {
  final Color color;
  final IconData icon;
  final String text;
  final String numbers;
  const DashboardCard({
    Key? key,
    required this.color,
    required this.icon,
    required this.text,
    required this.numbers,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: projectFieldIcons,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        height: context.deviceHeight() * 0.18,
        child: Row(
          children: [
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: color.withOpacity(0.2),
                shape: BoxShape.circle,
              ),
              child: Align(
                alignment: Alignment.center,
                child: Icon(icon, color: color),
              ),
            ),
            const SizedBox(width: 20),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                Text(
                  text,
                  style: const TextStyle(
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                const SizedBox(height: 10),
                Text(
                  numbers,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: context.scaleFont(20),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                const SizedBox(height: 15),
              ],
            ),
            const Expanded(child: SizedBox()),
            const Icon(MyFlutterApp.arrowiosforwardoutline),
          ],
        ),
      ),
    );
  }
}
