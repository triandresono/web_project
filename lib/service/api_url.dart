class UrlAPI {
  final vccIdentity = "http://54.151.160.33:8010/vcc/api/identity/v1";
  final vccMaster = "http://54.151.160.33:8020/vcc/api/master/v1";

  final commonHeader = {
    "Accept": "*/*",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": "true",
    "Access-Control-Allow-Headers":
        "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,locale",
    "Access-Control-Allow-Methods": "POST, OPTIONS"
  };
}
