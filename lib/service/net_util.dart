import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:project_web/helper/app_exception.dart';

class NetworkUtil {
  static NetworkUtil instance = NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => instance;

  Future<dynamic> testPost(String url,
      {Map<String, dynamic>? body,
      Map<String, String>? headers,
      Map<String, String>? param,
      encoding}) async {
    String jsonBody = json.encode(body);
    Uri uri = _setParameter(url, param);
    var responseAPI = await http.post(uri,
        headers: headers, body: jsonBody, encoding: encoding);
    var handledResp = _returnResponse(responseAPI);
    return handledResp;
  }

  Future<dynamic> post(String url,
      {Map<String, dynamic>? body,
      Map<String, String>? headers,
      Map<String, String>? param,
      encoding}) async {
    String jsonBody = json.encode(body);
    return await http
        .post(_setParameter(url, param),
            headers: headers, body: jsonBody, encoding: encoding)
        .then(
          (http.Response response) => _returnResponse(response),
        );
  }

  Future<dynamic> put(String url,
      {Map<String, dynamic>? body,
      Map<String, String>? headers,
      Map<String, String>? param,
      encoding}) async {
    String jsonBody = json.encode(body);
    return await http
        .put(_setParameter(url, param),
            headers: headers, body: jsonBody, encoding: encoding)
        .then(
          (http.Response response) => _returnResponse(response),
        );
  }

  Future<dynamic> postGetHeader(String url,
      {Map<String, String>? body,
      Map<String, String>? headers,
      Map<String, String>? param,
      encoding}) async {
    String jsonBody = json.encode(body);
    return await http
        .post(_setParameter(url, param),
            headers: headers, body: jsonBody, encoding: encoding)
        .then(
          (http.Response response) => _returnResponseWithHeader(response),
        );
  }

  Future<dynamic> posts(String url,
      {Map? body,
      Map<String, String>? headers,
      Map<String, String>? param,
      encoding}) async {
    String jsonBody = json.encode(body);
    return await http
        .post(_setParameter(url, param),
            headers: headers, body: jsonBody, encoding: encoding)
        .then(
          (http.Response response) => _returnResponse(response),
        );
  }

  Future<dynamic> get(String url,
      {Map<String, String>? param, Map<String, String>? headers, encoding}) {
    return http.get(_setParameter(url, param), headers: headers).then(
          (http.Response response) => _returnResponse(response),
        );
  }

  Future<dynamic> getImage(String url,
      {Map<String, String>? param, Map<String, String>? headers, encoding}) {
    return http.get(_setParameter(url, param), headers: headers).then(
          (http.Response response) => _returnResponseImage(response),
        );
  }

  Future<dynamic> getWithHeader(String url,
      {Map<String, String>? param, Map<String, String>? headers, encoding}) {
    return http.get(_setParameter(url, param), headers: headers).then(
          (http.Response response) => _returnResponseWithHeader(response),
        );
  }

  Future<dynamic> multipartOneFile(String req, String url,
      {File? image,
      Map<String, String>? headers,
      Map<String, String>? param,
      Map<String, String>? body,
      encoding}) async {
    Uri uri = _setParameter(url, param);
    var request = http.MultipartRequest(req, uri);
    if (headers != null && headers.isNotEmpty) request.headers.addAll(headers);
    if (body != null && body.isNotEmpty) request.fields.addAll(body);
    if (image != null) {
      request.files.add(
        await http.MultipartFile.fromPath(
          'file',
          image.path,
        ),
      );
    }
    http.StreamedResponse response = await request.send();
    final responseGetter = await response.stream.bytesToString();

    switch (response.statusCode) {
      case 200:
        Map responseJson = {};
        var responseHeader = response.headers;
        responseJson.addAll(responseHeader);

        var responseBody = json.decode(responseGetter);
        if (responseBody is Map) responseJson.addAll(responseBody);

        return responseJson;
      case 400:
        throw BadRequestException('Error 400');
      case 401:
        throw RefreshTokenFailedException('Response 401');
      case 403:
        throw UnauthorisedException(response.headers.toString());
      case 500:
        Map responseJson = {};
        responseJson.addAll(response.headers);
        var responseBody = json.decode(responseGetter);
        responseJson.addAll(responseBody);
        var message = responseJson['message'];
        throw FetchDataException('Error 500 : $message');
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  Future<dynamic> delete(String url,
      {Map<String, String>? headers,
      Map<String, String>? param,
      encoding}) async {
    return await http.delete(_setParameter(url, param), headers: headers).then(
          (http.Response response) => _returnResponse(response),
        );
  }

  dynamic _returnResponseWithHeader(http.Response response) {
    switch (response.statusCode) {
      case 200:
        Map responseJson = {};
        var responseBody = json.decode(response.body.toString());
        var responseHeader = response.headers;
        if (responseBody is Map) {
          responseJson.addAll(responseBody);

          responseJson.addAll(responseHeader);
        }
        dynamic returnThis;
        if (responseJson == {}) {
          returnThis = responseBody;
        } else {
          returnThis = responseJson;
        }
        return returnThis;
      case 400:
        var responseJson = json.decode(response.body.toString());
        throw BadRequestException(responseJson['message']);
      case 401:
        throw RefreshTokenFailedException('Response 401');
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        var responseJson = json.decode(response.body.toString());
        throw FetchDataException(responseJson['message']);
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  dynamic _returnResponseImage(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson;
        responseJson = response.body;
        return responseJson;
      case 400:
        dynamic responseJson = json.decode(response.body.toString());
        throw BadRequestException(responseJson['message']);
      case 401:
        throw RefreshTokenFailedException('Response 401');
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        var responseJson = json.decode(response.body.toString());
        throw FetchDataException(responseJson['message']);
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson;
        if (response.body is String && response.body.isNotEmpty) {
          responseJson = json.decode(response.body.toString());
        } else {
          responseJson = response.body;
        }
        return responseJson;
      case 400:
        var responseJson = json.decode(response.body.toString());
        throw BadRequestException(responseJson['message']);
      case 401:
        throw RefreshTokenFailedException('Response 401');
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        var responseJson = json.decode(response.body.toString());
        throw FetchDataException(responseJson['message']);
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

  Uri _setParameter(String url, Map<String, dynamic>? param) {
    int count = 0;
    if (param != null) {
      if (!url.endsWith('?')) {
        url = url + '?';
      }
      param.forEach((key, value) {
        if (count == 0) {
          url = url + key + '=$value';
          count++;
        } else {
          url = url + '&' + key + '=$value';
        }
      });
    }
    return Uri.parse(url);
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
