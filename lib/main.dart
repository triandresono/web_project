import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:project_web/helper/app_router.gr.dart';
import 'package:project_web/helper/constant.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const Routes(child: MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final route = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: route.defaultRouteParser(),
      routerDelegate: AutoRouterDelegate(route),
      title: 'Project Web',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Lato'),
    );
  }
}
