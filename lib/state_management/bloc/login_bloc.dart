import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:project_web/database/database_helper.dart';
import 'package:project_web/state_management/event/login_event.dart';
import 'package:project_web/state_management/state/login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    try {
      // var db = DatabaseHelper();
      if (event is LoginSubmit) {
        yield LoginInProgress();
        await Future.delayed(const Duration(seconds: 2));
        yield LoginSuccess();
      }
    } catch (e) {
      yield LoginFailed(e.toString());
    }
  }
}
