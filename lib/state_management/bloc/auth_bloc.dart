import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_web/database/database_helper.dart';
import 'package:project_web/state_management/event/auth_event.dart';
import 'package:project_web/state_management/state/auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(AuthInitial());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    try {
      DatabaseHelper db = DatabaseHelper();
      if (event is AuthLoggedIn) {
        bool isLoggedIn = await db.isLogged();
        if (isLoggedIn) {
          yield AuthLoggedIn();
        } else {
          yield AuthLoggedOut();
        }
      }
    } catch (e) {
      yield AuthError(e.toString());
    }
  }
}
