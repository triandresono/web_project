import 'package:flutter/material.dart';
import 'package:project_web/theme/colors.dart';

ButtonStyle confirmStyle(bool? isNull) {
  return TextButton.styleFrom(
    primary: projectWhite,
    minimumSize: const Size(88, 44),
    padding: const EdgeInsets.symmetric(horizontal: 16.0),
    shape: RoundedRectangleBorder(
        side: BorderSide(
          color: isNull! ? projectPrimary : projectDarkGray,
          width: 1.8,
        ),
        borderRadius: BorderRadius.circular(4)),
    backgroundColor: projectPrimary,
  );
}

ButtonStyle cancelStyle(bool? isNull) {
  return TextButton.styleFrom(
    primary: projectPrimary,
    minimumSize: const Size(88, 44),
    padding: const EdgeInsets.symmetric(horizontal: 16.0),
    shape: RoundedRectangleBorder(
      side: BorderSide(
        color: isNull! ? projectPrimary : projectDarkGray,
        width: 0.8,
      ),
      borderRadius: BorderRadius.circular(4),
    ),
    backgroundColor: projectWhite,
  );
}
