import 'dart:math';
import 'package:project_web/helper/app_scale.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:project_web/helper/constant.dart';
import 'package:project_web/theme/colors.dart';
import 'package:flutter/foundation.dart';

class Chart extends StatefulWidget {
  const Chart({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ChartState();
}

class ChartState extends State<Chart> {
  DateTime now = DateTime.now();
  int lastday = 0;
  final Color dark = const Color(0xff3b8c75);
  final Color normal = const Color(0xff64caad);
  final Color light = const Color(0xff73e8c9);
  int touchInt = -1;
  List<int> ints = [];

  @override
  initState() {
    super.initState();
    lastday = DateTime(now.year, now.month + 1, 0).day;
    for (int i = 0; i < lastday; i++) {
      var rng = Random();
      int x = rng.nextInt(50);
      ints.add((x < 10) ? 5 : x);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: (kIsWeb && !isWebMobile) ? 4 / 3 : 3 / 4,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(right: 24, bottom: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 28.0),
                child: Text(
                  'Jumlah Laporan',
                  style: TextStyle(
                    color: projectLightGray,
                    fontSize: context.scaleFont(12),
                  ),
                ),
              ),
              // const SizedBox(height: 30),
              Expanded(
                child: RotatedBox(
                  quarterTurns: (kIsWeb && !isWebMobile) ? 0 : 1,
                  child: BarChart(
                    BarChartData(
                      // alignment: BarChartAlignment.start,
                      titlesData: FlTitlesData(
                        show: true,
                        bottomTitles: SideTitles(
                          showTitles: true,
                          getTextStyles: (context, value) => TextStyle(
                            color: projectLightGray,
                            fontSize: context.scaleFont(10),
                          ),
                          getTitles: (double value) {
                            for (int i = 1; i < lastday; i++) {}
                            return (value + 1).toInt().toString();
                          },
                        ),
                        leftTitles: SideTitles(
                            showTitles: true,
                            reservedSize: 40,
                            interval: 10,
                            getTextStyles: (context, value) => TextStyle(
                                color: projectLightGray,
                                fontSize: context.scaleFont(10)),
                            margin: 0,
                            getTitles: (value) {
                              return (value.toInt().toString());
                            }),
                        topTitles: SideTitles(showTitles: false),
                        rightTitles: SideTitles(showTitles: false),
                      ),
                      gridData: FlGridData(
                        show: true,
                        // checkToShowHorizontalLine: (value) => value % 10 == 0,
                        verticalInterval: 10,
                        getDrawingHorizontalLine: (value) => FlLine(
                          color: const Color(0xffe7e8ec),
                          strokeWidth: 2,
                        ),
                      ),
                      borderData: FlBorderData(
                        show: false,
                      ),
                      groupsSpace: 25,
                      barGroups: getData(),
                      barTouchData: BarTouchData(
                        touchTooltipData: BarTouchTooltipData(
                            tooltipBgColor: Colors.blueGrey,
                            getTooltipItem: (group, groupIndex, rod, rodIndex) {
                              return BarTooltipItem(
                                (rod.y - 1).toInt().toString(),
                                const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                ),
                              );
                            }),
                        touchCallback: (FlTouchEvent event, touch) {
                          setState(
                            () {
                              if (!event.isInterestedForInteractions ||
                                  touch == null ||
                                  touch.spot == null) {
                                touchInt = -1;
                                return;
                              }
                              touchInt = touch.spot!.touchedBarGroupIndex;
                            },
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<BarChartGroupData> getData() {
    List<BarChartGroupData> bar = [];

    for (int i = 0; i < ints.length; i++) {
      List<BarChartRodData> list = [];
      list.add(BarChartRodData(
        y: ints[i].toDouble(),
        colors: i == touchInt ? [projectPrimary] : [projectLightGray],
        borderRadius: const BorderRadius.all(Radius.zero),
        backDrawRodData: BackgroundBarChartRodData(
          show: true,
          y: 51,
          colors: [projectWhite],
        ),
      ));
      BarChartGroupData data = BarChartGroupData(
        x: i,
        barsSpace: 4,
        barRods: list,
      );

      bar.add(data);
    }

    return bar;
  }
}
