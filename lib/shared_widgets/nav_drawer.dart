import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:project_web/helper/app_router.gr.dart';
import 'package:project_web/helper/app_scale.dart';
import 'package:project_web/helper/constant.dart';
import 'package:project_web/shared_widgets/expandeds.dart';
import 'package:project_web/theme/colors.dart';
import 'package:project_web/theme/icons.dart';
import 'package:project_web/helper/extensions.dart';

class NavDrawer extends StatefulWidget {
  const NavDrawer({Key? key}) : super(key: key);

  @override
  _NavDrawerState createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: projectWhite,
      width: MediaQuery.of(context).size.width * 0.62,
      child: Drawer(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 1.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Menu",
                        style: TextStyle(
                          fontSize: context.scaleFont(13),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Container(
                          padding: const EdgeInsets.only(
                            top: 8,
                            bottom: 8,
                            left: 8,
                            right: 1,
                          ),
                          child: Icon(
                            Icons.close,
                            size: context.scaleFont(17),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ChildTile(
                  icon: MyFlutterApp.homeoutline,
                  text: 'Dashboard',
                  onTap: () {
                    context.homeRoute(const DashBoardRoute());
                  },
                ),
                HeaderTile(
                  icon: MyFlutterApp.personoutline,
                  text: 'Manajemen Pengguna',
                  childrens: [
                    ChildTile(
                      text: 'Pengguna',
                      onTap: () {
                        context.homeRoute(const UsersRoute());
                      },
                    ),
                    ChildTile(
                      text: 'Menu',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Jaga',
                      onTap: () {},
                    ),
                  ],
                ),
                HeaderTile(
                  icon: MyFlutterApp.folderoutline,
                  text: 'Master',
                  childrens: [
                    ChildTile(
                      text: 'Parameter',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Informasi',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Informasi Checker',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Master Hari Libur',
                      onTap: () {},
                    ),
                  ],
                ),
                HeaderTile(
                  icon: MyFlutterApp.fileoutline,
                  text: 'Jaga',
                  childrens: [
                    ChildTile(
                      text: 'Input Laporan',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Verifikasi Data',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Investigasi',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Investigasi Checker',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Review LHI',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Recovery & Payment Notes',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'SLA Monitoring',
                      onTap: () {},
                    ),
                  ],
                ),
                HeaderTile(
                  icon: Icons.poll_outlined,
                  text: 'Laporan',
                  childrens: [
                    ChildTile(
                      text: 'Laporan LH',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Laporan Sanksi',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Laporan Periode Detail & Summary',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Laporan Custom All Data',
                      onTap: () {},
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget? navDrawer() {
  if (kIsWeb && !isWebMobile) {
    return null;
  } else {
    return const NavDrawer();
  }
}
