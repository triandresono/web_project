import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:project_web/helper/app_scale.dart';

final List<String> imgList = [
  'assets/img/image1.jpg',
  'assets/img/image2.jpg',
  'assets/img/image3.jpg',
];

final List<SliderArgs> dummy = [
  SliderArgs(image: 'assets/img/image1.jpg', text: ''),
  SliderArgs(image: 'assets/img/image2.jpg', text: dummy1),
  SliderArgs(image: 'assets/img/image3.jpg')
];

final themeMode = ValueNotifier(2);

final List<Widget> imageSliders = imgList
    .map(
      (item) => Container(
        margin: const EdgeInsets.all(5.0),
        child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            child: Stack(
              children: <Widget>[
                Image.asset(item,
                    fit: BoxFit.cover,
                    height: double.infinity,
                    width: double.infinity),
                // Positioned(
                //   bottom: 0.0,
                //   left: 0.0,
                //   right: 0.0,
                //   child: Container(
                //     decoration: const BoxDecoration(
                //       gradient: LinearGradient(
                //         colors: [
                //           Color.fromARGB(200, 0, 0, 0),
                //           Color.fromARGB(0, 0, 0, 0)
                //         ],
                //         begin: Alignment.bottomCenter,
                //         end: Alignment.topCenter,
                //       ),
                //     ),
                //     child: Text(
                //       'No. ${imgList.indexOf(item)} image',
                //       style: const TextStyle(
                //         color: Colors.white,
                //         fontSize: 20.0,
                //         fontWeight: FontWeight.bold,
                //       ),
                //     ),
                //   ),
                // ),
              ],
            )),
      ),
    )
    .toList();

class CarouselWithIndicatorDemo extends StatefulWidget {
  const CarouselWithIndicatorDemo({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicatorDemo> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CarouselSlider(
          items: imageSliders,
          carouselController: _controller,
          options: CarouselOptions(
            height: context.deviceHeight(),
            autoPlay: true,
            aspectRatio: 3 / 4,
            viewportFraction: 1.2,
            onPageChanged: (index, reason) {
              setState(
                () {
                  _current = index;
                },
              );
            },
          ),
        ),
        Positioned(
          bottom: 45.0,
          left: 0.0,
          right: 0.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: imgList.asMap().entries.map((entry) {
              return GestureDetector(
                onTap: () => _controller.animateToPage(entry.key),
                child: Container(
                  width: 12.0,
                  height: 12.0,
                  margin: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 4.0,
                  ),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: (Theme.of(context).brightness == Brightness.dark
                            ? Colors.grey
                            : Colors.white)
                        .withOpacity(_current == entry.key ? 0.9 : 0.4),
                  ),
                ),
              );
            }).toList(),
          ),
        ),
      ],
    );
  }
}

class SliderArgs {
  String? image;
  String? text;

  SliderArgs({this.image, this.text});
}

const String dummy1 =
    'Bank BTPN Whistleblowing System In an effort to improve the effectiveness of Anti-Fraud Strategy and Good Corporate Governance, as well as maintaining the trust and reputation of the company for all stakeholders (employees, customers, partners, etc), Bank BTPN provides a communication tool for both internal and external parties to report violation or fraudulent acts done by Bank BTPN internal employees through the whistleblowing system.Bank BTPN guarantees confidentiality of the identity of the whistleblower in order to allow them to feel secure while filing the alleged wrongdoings.Reports submitted must be related to fraud or violation of laws, code of conduct, and conflict of interests Whistleblowers must provide their identity, including name (anonymous allowed), and active phone number/email.';
