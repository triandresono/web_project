import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:project_web/helper/app_router.gr.dart';
import 'package:project_web/helper/app_scale.dart';
import 'package:project_web/helper/constant.dart';
import 'package:project_web/shared_widgets/expandable_drawer.dart';
import 'package:project_web/shared_widgets/expandeds.dart';
import 'package:project_web/theme/colors.dart';
import 'package:project_web/theme/icons.dart';
import 'package:project_web/helper/extensions.dart';

class PersistDrawer extends StatefulWidget {
  const PersistDrawer({Key? key}) : super(key: key);

  @override
  _PersistDrawerState createState() => _PersistDrawerState();
}

class _PersistDrawerState extends State<PersistDrawer> {
  bool isExpanded = true;
  List<bool> initial = [];
  @override
  void initState() {
    for (int i = 0; i < 5; i++) {
      initial.add(false);
    }
    WidgetsBinding.instance!.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isExpanded
        ? ExpandableWidget(
            direction: Axis.horizontal,
            expand: isExpanded,
            child: Container(
              color: projectWhite,
              width: context.deviceWidth() * 0.20,
              child: Drawer(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 1.0),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Menu",
                                style: TextStyle(
                                  fontSize: context.scaleFont(13),
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isExpanded = !isExpanded;
                                    for (int i = 0; i < initial.length; i++) {
                                      initial[i] = false;
                                    }
                                  });
                                },
                                child: Container(
                                  padding: const EdgeInsets.only(
                                    top: 8,
                                    bottom: 8,
                                    left: 8,
                                    right: 1,
                                  ),
                                  child: Icon(
                                    Icons.close,
                                    size: context.scaleFont(17),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        ChildTile(
                          icon: MyFlutterApp.homeoutline,
                          text: 'Dashboard',
                          onTap: () {
                            context.homeRoute(const DashBoardRoute());
                          },
                        ),
                        HeaderTile(
                          initialExpanded: initial[0],
                          icon: MyFlutterApp.personoutline,
                          text: 'Manajemen Pengguna',
                          childrens: [
                            ChildTile(
                              text: 'Pengguna',
                              onTap: () {
                                context.homeRoute(const UsersRoute());
                              },
                            ),
                            ChildTile(
                              text: 'Menu',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Jaga',
                              onTap: () {},
                            ),
                          ],
                        ),
                        HeaderTile(
                          initialExpanded: initial[1],
                          icon: MyFlutterApp.folderoutline,
                          text: 'Master',
                          childrens: [
                            ChildTile(
                              text: 'Parameter',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Informasi',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Informasi Checker',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Master Hari Libur',
                              onTap: () {},
                            ),
                          ],
                        ),
                        HeaderTile(
                          initialExpanded: initial[2],
                          icon: MyFlutterApp.fileoutline,
                          text: 'Jaga',
                          childrens: [
                            ChildTile(
                              text: 'Input Laporan',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Verifikasi Data',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Investigasi',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Investigasi Checker',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Review LHI',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Recovery & Payment Notes',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'SLA Monitoring',
                              onTap: () {},
                            ),
                          ],
                        ),
                        HeaderTile(
                          initialExpanded: initial[3],
                          icon: Icons.poll_outlined,
                          text: 'Laporan',
                          childrens: [
                            ChildTile(
                              text: 'Laporan LH',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Laporan Sanksi',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Laporan Periode Detail & Summary',
                              onTap: () {},
                            ),
                            ChildTile(
                              text: 'Laporan Custom All Data',
                              onTap: () {},
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ))
        : Container(
            color: projectWhite,
            width: MediaQuery.of(context).size.width * 0.035,
            child: Drawer(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 5,
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        MyFlutterApp.menu2outline,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = !isExpanded;
                        });
                      },
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        MyFlutterApp.homeoutline,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          context.navigateNamedTo(Routes.dashboard);
                        });
                      },
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        MyFlutterApp.personoutline,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          initial[0] = true;
                        });
                      },
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        MyFlutterApp.folderoutline,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          initial[1] = true;
                        });
                      },
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        MyFlutterApp.fileoutline,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          initial[2] = true;
                        });
                      },
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        Icons.poll_outlined,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          initial[3] = true;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
  }
}
