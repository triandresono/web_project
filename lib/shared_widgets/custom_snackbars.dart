import 'package:flutter/material.dart';
import 'package:project_web/theme/colors.dart';

successSnackBar(String message) {
  return SnackBar(
    duration: const Duration(seconds: 5),
    content: Text(
      message,
      style: const TextStyle(color: projectWhite, fontSize: 16),
    ),
    backgroundColor: projectSuccess,
  );
}

failSnackBar(String message) {
  return SnackBar(
    duration: const Duration(seconds: 5),
    content: Text(
      message,
      style: const TextStyle(color: projectWhite, fontSize: 16),
    ),
    backgroundColor: projectWarning,
  );
}
