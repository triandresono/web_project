import 'package:flutter/material.dart';
import 'package:project_web/shared_widgets/more_expanded.dart';
import 'package:project_web/theme/colors.dart';
import 'package:project_web/helper/app_scale.dart';
import 'package:project_web/theme/icons.dart';

class HeaderTile extends StatefulWidget {
  final String? text;
  final IconData? icon;
  final List<Widget>? childrens;
  final bool? initialExpanded;
  const HeaderTile({
    Key? key,
    this.icon,
    this.childrens,
    this.text,
    this.initialExpanded,
  }) : super(key: key);

  @override
  State createState() => HeaderTileState();
}

class HeaderTileState extends State<HeaderTile> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      initiallyExpanded: widget.initialExpanded ?? false,
      leading: Icon(
        widget.icon ?? MyFlutterApp.personoutline,
        // color: (isExpanded) ? projectWhite : projectBlack,
        size: context.scaleFont(15),
      ),
      title: Transform.translate(
        offset: const Offset(-16, 0),
        child: Text(
          widget.text ?? "---",
          style: TextStyle(
            // color: (isExpanded) ? projectWhite : projectBlack,
            fontWeight: FontWeight.w100,
            fontSize: context.scaleFont(12),
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ),
      children: widget.childrens ?? [],
      onExpansionChanged: (bool expanding) => setState(
        () => isExpanded = expanding,
      ),
    );
  }
}

class ChildTile extends StatefulWidget {
  final String? text;
  final IconData? icon;
  final Function()? onTap;
  const ChildTile({Key? key, this.text, this.icon, this.onTap})
      : super(key: key);

  @override
  _ChildTileState createState() => _ChildTileState();
}

class _ChildTileState extends State<ChildTile> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: (widget.icon != null)
          ? Icon(
              MyFlutterApp.homeoutline,
              size: context.scaleFont(15),
            )
          : const SizedBox(),
      title: Transform.translate(
        offset: const Offset(-16, 0),
        child: Text(
          widget.text ?? 'Pengguna',
          style: TextStyle(
              fontWeight: FontWeight.w100,
              color: projectBlack,
              fontSize: context.scaleFont(12)),
        ),
      ),
      onTap: widget.onTap ?? () {},
    );
  }
}

class ExpandedsTile extends StatefulWidget {
  final String? text;
  final IconData? icon;
  final List<Widget>? childrens;
  const ExpandedsTile({
    Key? key,
    this.text,
    this.icon,
    this.childrens,
  }) : super(key: key);

  @override
  _ExpandedsTileState createState() => _ExpandedsTileState();
}

class _ExpandedsTileState extends State<ExpandedsTile> {
  @override
  Widget build(BuildContext context) {
    return ConfigurableExpansionTile(
      borderColorStart: projectWhite,
      borderColorEnd: projectWhite,
      animatedWidgetFollowingHeader: const Icon(
        Icons.expand_more,
        color: projectFieldIcons,
      ),
      header: Flexible(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(
                widget.icon ?? MyFlutterApp.homeoutline,
                size: context.scaleFont(15),
              ),
              Text(
                widget.text ?? 'Pengguna',
                style: TextStyle(
                    fontWeight: FontWeight.w100,
                    color: projectBlack,
                    fontSize: context.scaleFont(13)),
              ),
            ],
          ),
        ),
      ),
      headerBackgroundColorStart: projectWhite,
      expandedBackgroundColor: projectWhite,
      headerBackgroundColorEnd: projectPrimary,
      bottomBorderOn: false,
      children: widget.childrens ?? [],
    );
  }
}
