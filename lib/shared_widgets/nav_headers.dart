import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:project_web/helper/constant.dart';
import 'package:project_web/theme/colors.dart';
import 'package:project_web/theme/icons.dart';
import 'package:project_web/helper/app_scale.dart';

AppBar navHeader(String titleAppBar, {List<Widget>? actions}) {
  return (kIsWeb && !isWebMobile)
      ? AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          title: Text(
            titleAppBar,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              color: projectBlack,
            ),
          ),
          backgroundColor: projectBG,
        )
      : AppBar(
          elevation: 0,
          automaticallyImplyLeading: false,
          title: Text(
            titleAppBar,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              color: projectBlack,
            ),
          ),
          leading: Builder(
            builder: (context) => IconButton(
              icon: const Icon(
                MyFlutterApp.menu2outline,
                color: projectBlack,
              ),
              onPressed: () => Scaffold.of(context).openDrawer(),
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            ),
          ),
          backgroundColor: projectBG,
        );
}

AppBar noDrawer(String titleAppBar, {List<Widget>? actions}) {
  return AppBar(
    elevation: 0,
    title: Text(
      titleAppBar,
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        color: projectBlack,
      ),
    ),
    backgroundColor: projectBG,
  );
}

// AppBar simpleHeader(String titleAppBar, {Function()? onBackPressed}) {
//   return AppBar(
//     title: Text(
//       titleAppBar,
//       style: const TextStyle(
//         fontWeight: FontWeight.bold,
//         color: projectPrimary,
//       ),
//     ),
//     leading: Builder(
//       builder: (context) => IconButton(
//         icon: const Icon(
//           Icons.arrow_back,
//           color: projectPrimary,
//         ),
//         // onPressed: onBackPressed ?? () => locator<NavigatorService>().goBack(),
//       ),
//     ),
//     backgroundColor: projectWhite,
//   );
// }

AppBar navHeaderTab(
    String titleAppBar, TabController controller, List<Widget> tabList) {
  return AppBar(
    title: Text(
      titleAppBar,
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.black,
        fontSize: 14,
      ),
    ),
    leading: Builder(
      builder: (context) => IconButton(
        icon: const Icon(
          Icons.menu,
          color: Colors.black,
        ),
        onPressed: () => Scaffold.of(context).openDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
    backgroundColor: Colors.blueAccent,
    bottom: TabBar(
        controller: controller,
        tabs: tabList,
        labelColor: Colors.blueAccent,
        labelStyle: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        unselectedLabelColor: Colors.grey,
        indicatorColor: Colors.black),
  );
}

AppBar navHeaderSearch({List<Widget>? actions, Widget? searchBar}) {
  return AppBar(
    title: searchBar,
    leading: Builder(
      builder: (context) => IconButton(
        icon: const Icon(
          Icons.menu,
          color: Colors.white,
        ),
        onPressed: () => Scaffold.of(context).openDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
    backgroundColor: Colors.lightBlue,
    actions: actions,
  );
}

// AppBar navComment(String titleAppBar,
//     {List<Widget> actions, dynamic returnval}) {
//   return AppBar(
//     title: Text(
//       titleAppBar,
//       style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
//     ),
//     leading: Container(
//       margin: EdgeInsets.only(right: 10),
//       height: 40,
//       width: 40,
//       child: Center(child: BtnBack(returnval: returnval)),
//     ),
//     backgroundColor: projectWhite,
//   );
// }

AppBar jagaBar() {
  return AppBar(
    automaticallyImplyLeading: false,
    flexibleSpace: const Image(
      image: AssetImage(imgPath + 'bar_mask.png'),
      fit: BoxFit.cover,
    ),
    toolbarHeight: (kIsWeb && !isWebMobile) ? 6.5.wh : null,
    elevation: 0,
    title: Builder(
      builder: (context) => Builder(
        builder: (context) => Row(
          children: [
            const SizedBox(width: 15),
            Image.asset(
              iconPath + 'jaga_logo.png',
              fit: BoxFit.contain,
            ),
          ],
        ),
      ),
    ),
    // leading: Builder(
    //   builder: (context) => Row(
    //     children: [
    //       Image.asset(
    //         iconPath + 'jaga_logo.png',
    //         fit: BoxFit.contain,
    //       ),
    //     ],
    //   ),
    // ),
    backgroundColor: projectPrimary,
  );
}
